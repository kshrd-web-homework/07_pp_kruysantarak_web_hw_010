import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import NavBar from './components/NavBar'
import './app.css'
import { Container } from "react-bootstrap";
import Author from "./pages/Author";

export default class App extends Component {

  render() {
    return (
      <div>
        <NavBar/>
          <Container>
          <Author/>
        </Container>
      </div>
    );
  }
}
