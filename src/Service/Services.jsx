import axios from "axios";

//Base URL
export const API = axios.create({
  baseURL: "http://110.74.194.124:3034/api",
});

//fetch all authors (done)
export const fetchAllAuthors = async () => {
  try {
    const result = await API.get("/author");
    console.log("fetchAllAuthors :", result);
    return result.data.data;
  } catch (error) {
    console.log("fetchAllAuthors Error : ", error);
  }
};

//fetch author by id (done)
export const fetchAuthorById = async (id) => {
  try {
    const result  = await API.get(`/author/${id}`)
    return result.data.data
  } catch (error) {
    console.log("fetchAuthorById Error : ", error)
  }
}

//delete category by id (done)
export const deleteAuthorById = async(id) => {
  try {
      const result = await API.delete(`/author/${id}`);
      return result.data.data;
    } catch (error) {
      console.log("deleteAuthorById Error : ", error);
    }
}

//upload image
export const uploadImage = async(image) => {
  try {
   const fd = new FormData();
   fd.append("image", image)
   const result = await API.post("/images", fd)
   console.log("uploadImage:", result.data.url);
   return result.data.url
  } catch (error) {
      console.log("uploadImage Error:", error);
  }
}

//Add author
export const addAuthor = async(author) => {
  try {
    const result = await API.post("/author", author);
    return result.data.data
  } catch (error) {
    console.log("addAuthor : ", error)
  }
}

//Update Author
export const updateAuthor = async(id, author) => {
  try {
    const result = await API.put(`/author/${id}`, author)
    return result.data.data
  } catch (error) {
    console.log("updateAuthor : ", error)
  }
}