import React, { useState, useEffect, useRef } from "react";
import { Form, Button } from "react-bootstrap";
import {
  fetchAllAuthors,
  deleteAuthorById,
  fetchAuthorById,
  addAuthor,
  updateAuthor,
  uploadImage,
} from "../Service/Services";

export default function Author() {
  const [authors, setAuthors] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [image, setImage] = useState("");
  const [previewImage, setPreviewImage] = useState("");
  const [onUpdate, setOnUpdate] = useState(false);
  const [loading, setLoading] = useState(false);
  const [updateId, setUpdateId] = useState("");
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [isValidName, setIsValidName] = useState(false);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const imageInputRef = useRef();
  const nameInputRef = useRef();
  const emailInputRef = useRef();

  const noImage = "https://www.pngitem.com/pimgs/m/80-801930_icon-gallery-red-png-clipart-png-download-red.png";

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchAllAuthors();
      setAuthors(result);
      setLoading(false)
    };
    fetchData();
  }, [loading]);

  //deleteAuthorById
  const onDeleteAuthor = async (id) => {
    const result = await deleteAuthorById(id);
    const temp = authors.filter((item) => {
      return item._id !== id;
    });
    setAuthors(temp);
  };

  //onAddAuthor
  const onAddAuthor = async (e) => {
    e.preventDefault();
    console.log("name : ", nameValidation())
    console.log("email : ", emailValidation())
    if(nameValidation() && emailValidation()){
      const imageURL = image && await uploadImage(image)
      const author = { 
        name, 
        email, 
        image : imageURL ? imageURL : noImage
      };
      const result = await addAuthor(author);
      setAuthors([author, ...authors])
      clearInput();
    } else {
      console.log("invalid")
      console.log("name validation : ", nameValidation())
      console.log("email validation : ", emailValidation())
    }
  };

  //onFetchAuthorById
  const onFetchAuthorById = async (id) => {
    const result = await fetchAuthorById(id);
    setName(result.name);
    setEmail(result.email);
    setPreviewImage(result.image)
    setUpdateId(id);
    setOnUpdate(true);
    setEmailError("");
    setNameError("");
  };

  //onUpdateAuthor
  const onUpdateAuthor = async (e) => {
    e.preventDefault();
    console.log(nameValidation());
    console.log(emailValidation());
    if(nameValidation() && emailValidation()){
      const imageURL = image && await uploadImage(image)
      const author = { 
        name, 
        email, 
        image : imageURL ? imageURL : noImage
      };
      const result = await updateAuthor(updateId, author);
      setOnUpdate(false);
      clearInput();
    } else {
      console.log("invalid")
      console.log("name validation : ", nameValidation())
      console.log("email validation : ", emailValidation())
    }
  };

  //clear input
  const clearInput = () => {
    setName("");        
    setEmail("");    
    setPreviewImage(""); 
    setEmailError("");   
    setNameError("");    
    setLoading(true)     
    imageInputRef.current.value = "";
  }

  //preview image 
  const onPreviewImage = (e) => {
    setImage(e.target.files[0]);
    setPreviewImage(URL.createObjectURL(e.target.files[0]));
  }

  //email validation 
  const emailValidation = () => {
    let isValid = true;
    let emailRegex = /^[^\s@]+@([^\s@.,]+\.)+[^\s@.,]{2,}$/;
    if( !emailRegex.test(emailInputRef.current.value)){
      setEmailError("Email is invalid.");
      setIsValidEmail(false);
      isValid = false;
    } else {
      setIsValidEmail(true);
      isValid = true;
    }
    return isValid;
  }

  //name validation
  const nameValidation = () => {
    let isValid = true;
    if(nameInputRef.current.value.trim() === ""){
      setNameError("Name is invalid.")
      setIsValidName(false);
      isValid = false;
    } else {
      setIsValidName(true);
      isValid = true;
    }
    return isValid;
  }

  return (
    <div>
      <div className="mb-2">
        <Form>
          <div className="row">
            <div className="col-lg-8 col-md-12">
              <h1>{onUpdate ? "Update Author" : "Create Author"}</h1>
              <Form.Group controlId="formBasicEmail" className="mb-3">
                <Form.Control
                  ref={nameInputRef}
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Author"
                />
                <p className="mt-1" style={{color: "red"}}>{isValidName ? "" : nameError}</p>
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  ref={emailInputRef}
                  type="text"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="Email"
                />
                <p className="mt-1" style={{color: "red"}}>{isValidEmail ? "" : emailError}</p>
              </Form.Group>
              <div className="d-flex">
                <Button
                  onClick={onAddAuthor}
                  className="my-3 me-2"
                  variant="primary"
                  type="submit"
                >
                  Add
                </Button>

                <Button
                  style={
                    onUpdate ? { display: "inline-block" } : { display: "none" }
                  }
                  onClick={onUpdateAuthor}
                  className="my-3"
                  variant="primary"
                  type="submit"
                >
                  Update
                </Button>
              </div>
            </div>
            <div className="col-lg-4 col-md-12">
            <img
                style={{width: "50%", margin: "auto"}}
                className="mb-3 d-block"
                src={previewImage ? previewImage : noImage}
                alt=""
              />
              <input
                name="file"
                id="imageFile"
                type="file"
                className="form-control mb-3"
                onChange={onPreviewImage}
                accept="image/*"
                ref={imageInputRef}
              />
            </div>
          </div>
        </Form>
      </div>
      <div>
        <table className="table" id="table-data">
          <thead>
            <tr className="bg-success text-light">
              <th scope="col" style={{ width: "15%" }}>#</th>
              <th scope="col" style={{ width: "25%" }}>Name</th>
              <th scope="col" style={{ width: "25%" }}>Email</th>
              <th scope="col" style={{ width: "20%" }}>Image</th>
              <th scope="col" style={{ width: "20%" }}>Action</th>
            </tr>
          </thead>
          <tbody>
            {authors.map((item, index) => (
              <tr key={item._id}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>
                  {" "}
                  <img src={item.image} alt="" width="80%" />
                </td>
                <td>
                  <button
                    onClick={() => onFetchAuthorById(item._id)}
                    type="button"
                    className="btn btn-warning mb-3"
                  >
                    Edit
                  </button>{" "}
                  <button
                    onClick={() => onDeleteAuthor(item._id)}
                    type="button"
                    className="btn btn-danger mb-3"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
